## Weather forecast service for corpus.by

Simple Flask app that gets current weather report 
by sending request to [OpenWeatherMap](https://openweathermap.org/current) API endpoint.

### Required credentials

In order to use OpenWeatherMap you have to 
[create a free account](https://home.openweathermap.org/users/sign_up)
and put your own API token under `src/env.py`:
* ```python 
  OPENWEATHERMAP_API_KEY = '...'
  ```

### To run locally (windows):

1. `> cd src`

2. `> set FLASK_APP=corpus`

3. `> flask run`

### Internationalization (i18n)

According to [flask-babel documentation](https://python-babel.github.io/flask-babel):

1. To extract string resources for i18n:<br>
  `> pybabel extract -F babel.cfg -o messages.pot .`

2. To init translation file for belarusian:<br>
  `> pybabel init -i messages.pot -d translations -l be`

3. To compile the translations:<br>
  `> pybabel compile -d translations`


To merge new text strings into existing `.po` tranlsation files:
  * Run the step (1) to update `messages.pot`
  * And let pybabel to merge changes using:<br>
       `pybabel update -i messages.pot -d translations`
       
       Afterwards some strings might be marked as **fuzzy** 
       (where it tried to figure out if a translation matched a changed key). 
       If you have fuzzy entries, make sure to check them by hand and remove the fuzzy flag before compiling.
  * Recompile translations using step (3)
